# README #

### What is this repository for? ###

* This is example code used to control an LED strip from a Website

### How do I get set up? ###

* This code assumes that you have already set up a MYSQL database with a table described by 
* `CREATE TABLE MYSQL_DB (
 light_on varchar(20) DEFAULT NULL,
 intensity int(11) DEFAULT NULL,
 pattern_on int(11) DEFAULT NULL,
 pattern varchar(20) DEFAULT NULL,
 speed double DEFAULT NULL,
 color_order varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;`

* with 4 initialized rows (light_on = “Red”, “Blue”, “Green”, Null).
*	`INSERT INTO MYSQL_DB (light_on, intensity) VALUES("Red", 0);`
*	`INSERT INTO MYSQL_DB (light_on, intensity) VALUES("Green", 0);`
*	`INSERT INTO MYSQL_DB (light_on, intensity) VALUES("Blue", 0);`
*	`INSERT INTO MYSQL_DB (pattern_on, pattern, speed, color_order) VALUES(0, "A PATTERN", .01, "Red Green Blue");`