import urllib, json, time, sys
sys.path.insert(0, 'INSERT LIBRARY PATH HERE')
import LEDLibrary

# I used ports 13, 19, and 26.  Feel free to use any GPIO port
colorToPin = {"Red": 26, "Blue": 19, "Green": 13}

# Insert the url of your server here
serverUrl = "INSERT URL HERE"
intensity = {"Red": 0, "Blue": 0, "Green": 0}
updated = False

#I initialize all led colors to be off to start
LEDLibrary.zero([colorToPin["Red"],colorToPin["Blue"],colorToPin["Green"]])

while(True):
    response = urllib.urlopen(serverUrl)
    data = json.loads(response.read())

    colorPattern = []
    changes = []
    patternOn = False
    ledPattern = ""
    speed = 0
    colorOrder = []
    for x in range (0,len(data)):
        color = data[x]['light_on']
        if(color is not None):
            colorPattern.append(colorToPin[color])

            val = int(data[x]['intensity'])
            changes.append([intensity[color],val])
            if(not(intensity[color] == val)):
                updated = True
            intensity[color] = val
        #If light_on == NULL, then pattern information
        else:
            dbPatternOn = int(data[x]['pattern_on'])
            if(dbPatternOn != 0):
                patternOn = True;
                ledPattern = data[x]['pattern']
                speed = float(data[x]['speed']);
                colorOutput = data[x]['color_order'];
                colorOutput = colorOutput.strip()
                colorArray = colorOutput.split(' ');
                for x in range (0, len(colorArray)):
                    colorOrder.append(colorToPin[colorArray[x]]);
    if(patternOn):
        LEDLibrary.zero([colorToPin["Red"],colorToPin["Blue"],colorToPin["Green"]])
        methodToCall = getattr(LEDLibrary, ledPattern);
        result = methodToCall(colorOrder, speed)
    else:
        LEDLibrary.modfadecolors(colorPattern, changes, speed)
