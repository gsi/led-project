<p id ="ledcolor" style ="background-color = rgb(0,0,0)">LED Color</p>
<p>
    <div>Red <input type="textbox" id="redVal"></input></div>
    <div>Green <input type="textbox" id="greenVal"></input></div>
    <div>Blue <input type="textbox" id="blueVal"></input></div>
</p>
<div>Speed?<input type="textbox" id="speedBox"></input></div>
<p>Pattern On? <input type="checkbox" id="patternON" onclick="patternONClick(this);" value=1></input></p>
<div id="patternOptions">
<p id='patternP'>Pattern<select id="whatpattern">
    <option value="turnzero">Turn off all lights</option>
    <option value="simplefade">Fade colors on and off</option>
    <option value="multifadecolors">Fades between colors</option>
    <option value="buildfade">Builds up colors</option>
    <option value="rainbow">Displays a rainbow</option>
</select></p>
<input type="checkbox" id='redCheck' value="Red" onclick="colorClick(this);">
Red</input>
<input type="checkbox" id='greenCheck' value="Green" onclick="colorClick(this);">Green</input>
<input type="checkbox" id='blueCheck' value="Blue" onclick="colorClick(this);">
Blue</input>
<button type="button" onclick="clearClick(this);">Clear Order</button></br>
<span id="order"></span>
</div>
<button type="button" onclick="submitClick();">Submit Changes</button>
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script>
$(document).ready(function(){
    patternONClick("#patternP");
});

$(window).click(function(){
    var redVal = parseInt($('#redVal').val());
    var blueVal = parseInt($('#blueVal').val());
    var greenVal = parseInt($('#greenVal').val());
    $('#ledcolor').css(
        "background-color", "rgb("+redVal+","+greenVal+","+blueVal+")");
});

function colorClick(color){
    if($(color).is(":checked")){
    var list = $("#order").text();
    list += " "+$(color).val();
    $('#order').html(list);
    $(color).attr('disabled', true);
    }
};

function clearClick(){
    $("#order").html("");
    $('#redCheck').attr('disabled', false);
    $('#greenCheck').attr('disabled', false);
    $('#blueCheck').attr('disabled', false);
    $('#redCheck').attr('checked', false);
    $('#greenCheck').attr('checked', false);
    $('#blueCheck').attr('checked', false);
}

function turnzeroClick(){
    $('#redVal').val(0);
    $('#blueVal').val(0);
    $('#greenVal').val(0);
}

function patternONClick(checkbox){
    if($(checkbox).is(":checked")){
        $('#patternOptions').show();
    }
    else{
        $('#patternOptions').hide();
    }
}

function submitClick(){
    var patternON = $('#patternON').is(":checked");
    ledpattern = $("#whatpattern option:selected").val();
    var redVals = $('#redVal').val();
    var greenVals = $('#greenVal').val();
    var blueVals = $('#blueVal').val();
    if(ledpattern == "turnzero" && patternON){
        redVals = 0;
        blueVals = 0;
        greenVals = 0;
        turnzeroClick();
    }
    var speed = $("#speedBox").val();
    var order = $("#order").html();
    data = {'redVal' : redVals,
            'greenVal' : greenVals, 
            'blueVal' : blueVals, 
            'patternON' : patternON, 
            'ledpattern': ledpattern, 
            'speed' : speed, 
            'order' : order};

    console.log(data);
    updateRecord(data);
}

function updateRecord(data){
    jQuery.ajax({
    type: "post",
    url: "update.php",
    data: data,
    cache: false,
    success: function(response){
        alert("Record successfully updated");
    },
    error: function(response){
        console.log("ERROR");
    }});
}
</script>
