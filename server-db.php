<?php

$con = mysqli_connect('localhost','INSERT DB USR HERE','INSERT PSWRD HERE','INSERT DB NAME HERE');
if (!$con) {
    die('Could not connect: ' . mysqli_error($con));
}

$colorNames = array("Red", "Green", "Blue");
//Make sure to update this list when more patterns are added
$patterns = array("turnzero", "simplefade", "multifadecolors", "buildfade", "rainbow");

$vals = array(
    'Red' => intval($_POST['redVal']),
    'Green' => intval($_POST['greenVal']),
    'Blue' => intval($_POST['blueVal']),
    'patternON' => $_POST['patternON'],
    'ledpattern' => $_POST['ledpattern'],
    'speed' => $_POST['speed'],
    'color_order' => $_POST['order'],
);    

foreach($vals as $key => $val){
    $invalidInput = false;

    $sql="UPDATE INSERT DB HERE set ";
    if(in_array($key, $colorNames)){
        if(is_int($val)){ 
            if($val < 0){
                $val = 0;
            }
            elseif ($val > 255){
                $val = 255;
            }
            $sql .= "intensity = ".$val." WHERE light_on = \"".$key."\";";
        }
        else{
            error_log("Invalid RGB val");
            $invalidInput = true;
        }
    }   
    elseif($key == 'patternON'){
        $bool = 0;
        if($val == "true"){
            $bool = 1;
        }
        //THIS WILL BREAK IF MULTIPLE NON NULL PATTERN ROWS EXIST
        $sql .= "pattern_on = ".$bool." WHERE pattern_on IS NOT NULL;";
    }
    elseif ($key == "ledpattern"){
        //THIS WILL BREAK IF MULTIPLE NON NULL PATTERN ROWS EXIST
        if(in_array($val, $patterns)){
            $sql .= "pattern = \"".$val."\" WHERE pattern IS NOT NULL;";
        }
        else{
            error_log("Invalid pattern");
            $invalidInput = true;
        }
    }
    elseif ($key == "speed"){
        //THIS WILL BREAK IF MULTIPLE NON NULL PATTERN ROWS EXIST
        if(is_numeric($val)){
            $sql .= "speed = \"".$val."\" WHERE pattern IS NOT NULL;";
        }
        elseif($val == ""){
            $sql .= "speed = .01 WHERE pattern IS NOT NULL;";
        }
        else{
            error_log("Invalid speed ".$val);
            $invalidInput = true;
        }
    }
    elseif ($key == "color_order"){
        //THIS WILL BREAK IF MULTIPLE NON NULL PATTERN ROWS EXIST
        $colors = array_slice(explode(" ",$val), 1);
        if(sizeOf($colors) > 0){
            foreach($colors as $color){
                if(!(in_array($color, $colorNames))){
                    error_log("Invalid color name ".print_r($colors, true));
                    $invalidInput = true;
                    break;
                }
            }
        }
        else{
            $colors = $colorNames;
        }
        $sql .= "color_order = \"".implode(" ", $colors)."\" WHERE pattern IS NOT NULL;";
    }
    else{
        die ("Invalid val key");
    }
    if(!($invalidInput)){
        error_log($sql);
        $result = mysqli_query($con,$sql);
    }
}
