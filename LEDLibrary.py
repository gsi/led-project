# This library contains some pre-built patterns for the LEDs

# Import necessary libraries
import time, threading
import pigpio

pi = pigpio.pi()

# Turns off all lights
def zero(colors):
    if(len(colors) >= 1):
        pi.set_PWM_dutycycle(colors[0], 0)
    if(len(colors) >= 2):
        pi.set_PWM_dutycycle(colors[1], 0)
    if(len(colors) >= 3):
        pi.set_PWM_dutycycle(colors[2], 0)

#Simple fade in and out
#inout = 0,1,2  0= fade both, 1=fade in, 2=fade out
def fade(colors, speed, inout):
    val = 0
    if(inout != 2):
        while(val < 255):
            for x in range (0, len(colors)):
                pi.set_PWM_dutycycle(colors[x], val)
            val = val + 1
            time.sleep(speed)
    if(inout != 1):
        val = 255
        while(val > 0):
            for x in range (0,len(colors)):
                pi.set_PWM_dutycycle(colors[x], val)
            val = val - 1
            time.sleep(speed)

#Fades between 2 values
def fadevals(color, val1, val2, speed):
    inc = 1
    v1 = val1
    v2 = val2
    if(val1>val2):
        inc = -1
        v1 = val2
        v2 = val1
    while(v1 < v2):
        pi.set_PWM_dutycycle(color, val1)
        val1 = val1 + inc
        v1 = v1 + 1
        time.sleep(speed)


#Fades between 2 colors
def fadecolors(colors, start1, end1, start2, end2, speed):
    inc1 = 1
    inc2 = 1
    if(start1 > end1):
        inc1 = -1
    if(start2 > end2):
        inc2 = -1
    
    while(start1 != end1 or start2 != end2):
        if(start1 != end1):
            pi.set_PWM_dutycycle(colors[0], start1)
            start1 = start1 + inc1
        if(start2 != end2):
            pi.set_PWM_dutycycle(colors[1], start2)
            start2 = start2 + inc2
        time.sleep(speed)

#Fades between 2 colors with starting values saved in 4 element array
def modfadecolors(colors, vals, speed):
    numDone = [0,0,0]
    while(numDone[0] != 1 or numDone[1] != 1 or numDone[2] != 1):
        for x in range(0,len(colors)):
            if(numDone[x] == 0):
                if(vals[x][0] == vals[x][1]):
                    numDone[x] = 1
                else:
                    inc = 1
                    if (vals[x][0] > vals[x][1]):
                        inc = -1
                    pi.set_PWM_dutycycle(colors[x], vals[x][0])
                    vals[x][0] = vals[x][0] + inc
        time.sleep(speed)


#
# Processes
#
# Turns off all lights
def turnzero(colors, speed):
    if(len(colors) >= 1):
        pi.set_PWM_dutycycle(colors[0], 0)
    if(len(colors) >= 2):
        pi.set_PWM_dutycycle(colors[1], 0)
    if(len(colors) >= 3):
        pi.set_PWM_dutycycle(colors[2], 0)

#Simple fade in and out
def simplefade(colors, speed):
    val = 0
    while(val < 255):
        for x in range (0, len(colors)):
            pi.set_PWM_dutycycle(colors[x], val)
            val = val + 1
            time.sleep(speed)
    val = 254
    while(val > 0):
        for x in range (0,len(colors)):
            pi.set_PWM_dutycycle(colors[x], val)
            val = val - 1
            time.sleep(.01)

#Fades between multiple colors
def multifadecolors(colors, speed):
    start = 0
    limit = 255
    zero(colors)
    fadevals(colors[0], start, limit, speed)
    index = 1
    while(index < len(colors)):
        fading_colors = [colors[index-1], colors[index]]
        fadecolors(fading_colors, limit, start, start, limit, speed)
        index = index + 1
    fadevals(colors[index-1],limit,start,speed)
    zero(colors)

#Builds up to white light given a list of colors
def buildfade(colors, speed):
    zero(colors)
    colori = 0
    while (colori<len(colors)):
        fade([colors[colori]],speed,1)
        colori = colori + 1
    fade(colors, speed, 2)

#Displays a rainbow
#Colors must be in rgb format
def rainbow (colors, speed):
    if(not(len(colors) == 3)):
        return
    red=colors[0]
    green=colors[1]
    blue=colors[2]
    zero([red,green,blue])
    fade([red], speed, 1)
    time.sleep(1)
    fadevals(green,0,50,.01)
    time.sleep(3.05)
    fadevals(green,50,128,.01)
    time.sleep(2.77)
    fadecolors([red,green],255, 0,128,255,.01)
    time.sleep(1)
    fadecolors([green, blue],255, 0,0,255,.01)
    time.sleep(1)
    fadecolors([red,blue],0,100,255,200, .01)
    time.sleep(2.28)
    fadecolors([red,blue],100,128,200,128, .01)
    time.sleep(2.28)
    fadecolors([red,blue],128,0,128,0, .01)
